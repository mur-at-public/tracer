#!/usr/bin/env python3

import argparse, os, tempfile, subprocess
import cv2
import svg2ild

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("image", help="imagefile path")
    parser.add_argument("--t1", type=int, default=300,
        help="first threshold for the hysteresis procedure.")
    parser.add_argument("--t2", type=int, default=500,
        help="first threshold for the hysteresis procedure.")
    parser.add_argument("-b", "--bitmap", help="bitmap output path.")
    parser.add_argument("-s", "--svg",
        default=os.path.join(tempfile.gettempdir(), 'tracer_tmp.svg'),
        help="SVG output file path.")
    parser.add_argument("--raster-retracer",
        default=os.path.expanduser('~/.cargo/bin/raster-retrace'),
        help="location of the raster-retracer binary.")
    parser.add_argument("-i", "--ilda", help="ILDA output file path.")
    args = parser.parse_args()
    return args

def main(args):

    # kantenerkennung

    src = cv2.imread(args.image)                     # bild einlesen
    gray = cv2.cvtColor(src, cv2.COLOR_RGB2GRAY)     # in graustufen umwandeln
    edges = cv2.Canny(gray, args.t1, args.t2)        # konturen finden

    if args.bitmap:                                  # wenn bitmamp gewünscht wurde
        cv2.imwrite(args.bitmap, edges)              # speichern


    # SVG konvertierung
    
    tmp_ppm = (os.path.join(tempfile.gettempdir(), 'tracer_tmp.ppm'))
    if (not args.svg.endswith('tracer_tmp.svg')) or args.ilda:
        edges = cv2.bitwise_not(edges)                  # bild invertieren
        bgr = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)   # ppm export benötigt BRG
        cv2.imwrite(tmp_ppm, bgr)
        subprocess.run([args.raster_retracer, '-i', tmp_ppm, 
                        '-o', args.svg, '-m', 'CENTER'])

    # ILDA konvertierung

    if args.ilda:
        params = svg2ild.RenderParameters()
        frame = svg2ild.load_svg(args.svg)
        frame.sort()
        rframe = frame.render(params)
        svg2ild.write_ild(params, rframe, args.ilda)
        
        print("Statistics:")
        print(" Objects: %d"%params.objects)
        print(" Subpaths: %d"%params.subpaths)
        print(" Bezier subdivisions:")
        print("  Due to rate: %d"%params.rate_divs)
        print("  Due to flatness: %d"%params.flatness_divs)
        print(" Points: %d"%params.points)
        print("  Trip: %d"%params.points_trip)
        print("  Line: %d"%params.points_line)
        print("  Bezier: %d"%params.points_bezier)
        print("  Start dwell: %d"%params.points_dwell_start)
        print("  Curve dwell: %d"%params.points_dwell_curve)
        print("  Corner dwell: %d"%params.points_dwell_corner)
        print("  End dwell: %d"%params.points_dwell_end)
        print("  Switch dwell: %d"%params.points_dwell_switch)
        print(" Total on: %d"%params.points_on)
        print(" Total off: %d"%(params.points - params.points_on))
        print(" Efficiency: %.3f"%(params.points_on/float(params.points)))
        print(" Framerate: %.3f"%(params.rate/float(params.points)))

if __name__ == '__main__':
    args = parse_args()
    main(args)
