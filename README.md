# Konturen-Tracer für Laser-Projektionen

Das genz einfach gehaltene Python Script `tracer.py` fasst die notwendigen Bearbeitungsschritte zusammen, um Konturen in einem Bild für die Projektion mit Laser-Scannern aufzubreiten.

Die Umsezung erfolgt in drei Schritten:

1. Konturenerkennung Mittels OpenCV bzw. der [Canny Methode](https://docs.opencv.org/3.4/d7/de1/tutorial_js_canny.html)

2. Umwandlung der Linen in eine SVG Vektor-Grafik mit Hilfe von [raster-retrace](https://github.com/ideasman42/raster-retrace)

3. Umwandlung und Export der Linien als ILDA File. Das dafür verwendete Python Script `svg2ild.py` stammt aus dem [openlase](https://github.com/marcan/openlase) Projekt.

## Notwendige Installationen

Beide Python Scipts verlangen ein aktuelles Python3.
Zusätzlich muss die Pythonerweiterung: [`opencv-python`](https://pypi.org/project/opencv-python/) installiert sein.

Das relativ unbekannte `raster-retrace` wird verwendet, weil es im Gegesatz zu `potrace` eine bessere Unterstützung für _centerline tracing_ bietet. 
Wie alle Rust Programme, lässt es sich ausgesprochen einfach auf allen gängigen Betriebsystemen mit [installierter Rustumgebung](https://www.rust-lang.org/en-US/install.html) mit dem Cargo Package Manager installieren:

    cargo install raster-retrace

## Zur Bedienung

Das Script liefert mit dem Befehl `tracer --help` eine Übersicht über alle verfügbaren Komandozeilenparameter. Nur wenige davon sind im Normalfall nötig.
Die optionalen Parameter erlauben aber bspw. auch die Ausgebe von Zwischenschritten im Verarbeitungsablauf.

Ausgehend von einem beliebigen Bild, werden im ersten Verabeitungschritt die Konturen bestimmt bzw. ein S/W-Bild mit stark hervorgehobenen Grenzlinen gezeichnet.

also bspw. für das Bild:

![](examples/12-715.png)

generiert der Befehl:

    ./tracer.py -b examples/bitmap.png examples/12-715.png

folgendes Resultat:

![](examples/bitmap.png)

In der Praxis gilt es allerdings gerade hier mit den beiden _threshold_ Parametern `T1` (default: 300) und `T2` (default: 500) geeignete Einstellungen für das jeweilige Bild zu finden. Wenn die Werte nicht passen, werden entweder gar keine Konturen hervorgehoben, oder derart viele, dass sie die Ausgabemöglichkeiten des Laser-Scanners schnell übersteigen.

bspw.:

    ./tracer.py -b examples/bitmap2.png --t1 150 --t2 200 examples/12-715.png

![](examples/bitmap2.png)

Im nächsten Schritt werden diesen S/W-Bildern in SVG-Vektorgrafiken umgewandelt.

Auch dieses Zwichenresultat läst sich durch Angabe einer entsprechenen Zieldatei wieder ausgeben.

    
    ./tracer.py --svg examples/vectors.svg examples/12-715.png

![](examples/vectors.svg)

Schließlich kann auch direkt ein ILDA-File, wie es gewöhnlich zur Ansteuerung von Laser-Scannern verwendet wird generiert werden.

    ./tracer.py --ilda examples/output.ild examples/12-715.png

Dabei werden auch einige statistische Anhaltspunkte (Anzahler der Knotenpunkte, zu Erwartende Framerate etc.) ausgegeben, die eine grobe Beurteilung der praktischen Verwendung für die Projektion erlauben.

Das resulutierende ILDA-File --  [hier unser Beispiel](examples/output.ild) -- kann man auch in diversen Software Projektions-Simulatoren testen.

Diesen einfachen ILDA-Viewer kann man bspw. ohne aufwendige Installation direkt im Web-Browser nutzen:

https://rawgit.com/possan/ilda.js/master/examples/render-canvas.html
